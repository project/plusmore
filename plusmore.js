(function($) {
	$(document).ready(function(){
		$(".term-plus-more").click(function(){
			$(".term-plus-more").hide();
			$(this).next().slideToggle("slow");
			return false;
		}).next().hide();
		$(".more-term-hide").click(function(){
			$(".term-plus-data").hide();
			$(".term-plus-more").show();
		});
	});
})(jQuery);

