Plus More
=========================
This module provides a simple formatter for taxonomy terms to display the terms with plus more effect with a configurable items to be shown before +more. Eg : Drupal,Git +3 more.

Install
=========================
1) Simply drop this module into your modules directory
2) Install via admin/modules.
3) Select the display format "Display +more terms" in manage display.

